using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    public class BehaviourNewGraphEditor : EditorWindow
    {
        public static void ShowPopup(BehaviourGraphEditor graphEditor)
        {
            BehaviourNewGraphEditor window = ScriptableObject.CreateInstance<BehaviourNewGraphEditor>();
            window.titleContent = new GUIContent("Create new Behaviour Graph");
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 450, 250);
            window.graphEditor = graphEditor;
            window.ShowPopup();
        }

        //Graph
        private BehaviourGraphEditor graphEditor;
        private string graphName = "BehaviourGraph";

        //GUi
        private void OnGUI()
        {
            //GUILayoutUtility.GetRect(Screen.width, 60);
            GUI.Label(new Rect(5, 5, Screen.width, 50), "Create New Graph", BehaviourGraphStyles.GraphTitleStyle);

            //TextField
            GUI.Label(new Rect(15, 55, Screen.width, 50), "Graph Name", BehaviourGraphStyles.GraphTextFieldLabel);
            graphName = GUI.TextField(new Rect(15, 80, Screen.width - 30, 38), graphName, BehaviourGraphStyles.GraphTextField);

            //Close Button
            if (GUI.Button(new Rect(15, 140, (Screen.width / 2f) - 15, 80), "Cancel", BehaviourGraphStyles.CancelButton))
                this.Close();

            //Create
            if(GUI.Button(new Rect((Screen.width / 2f), 140, (Screen.width / 2f) - 15, 80), "Create", BehaviourGraphStyles.AcceptButton))
            {
                graphEditor.CreateNewGraph(graphName);
                this.Close();
            }

            Repaint();
        }
    }
}