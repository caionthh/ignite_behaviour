using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    public static class BehaviourGraphStyles
    {
        //Graph Title
        public static GUIStyle GraphTitleStyle { get {
                GUIStyle style = new GUIStyle();
                style.fontSize = 32;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;

                return style;
            } }
        public static GUIStyle GraphSubtitleStyle
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.fontSize = 20;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;

                return style;
            }
        }
        public static GUIStyle GraphTextFieldLabel
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.fontSize = 18;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;

                return style;
            }
        }
        public static GUIStyle GraphTextField
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.fontSize = 14;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;
                style.alignment = TextAnchor.MiddleLeft;
                style.padding = new RectOffset(5, 5, 1, 1);
                style.border = new RectOffset(12, 12, 12, 12);
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1.png") as Texture2D;

                return style;
            }
        }

        public static GUIStyle BigButton
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.fontSize = 16;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.white;
                style.alignment = TextAnchor.MiddleCenter;

                style.border = new RectOffset(12, 12, 12, 12);
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Gray.png") as Texture2D;

                return style;
            }
        }
        public static GUIStyle AcceptButton
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.fontSize = 14;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;
                style.alignment = TextAnchor.MiddleCenter;
                style.border = new RectOffset(12, 12, 12, 12);
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Gray.png") as Texture2D;
                style.hover.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Green.png") as Texture2D;

                return style;
            }
        }
        public static GUIStyle CancelButton
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.fontSize = 14;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;
                style.alignment = TextAnchor.MiddleCenter;
                style.border = new RectOffset(12, 12, 12, 12);
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Gray.png") as Texture2D;
                style.hover.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Red.png") as Texture2D;

                return style;
            }
        }

        //Node
        public static GUIStyle GraphNodeStyle
        {
            get
            {
                GUIStyle style = new GUIStyle();
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Gray.png") as Texture2D;
                style.border = new RectOffset(12, 12, 12, 12);

                return style;
            }
        }
        public static GUIStyle NodeTitleStyle
        {
            get
            {
                GUIStyle style = new GUIStyle();

                //Background
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1.png") as Texture2D;
                style.border = new RectOffset(12, 12, 12, 12);

                //Text
                style.fontSize = 16;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.gray;
                style.alignment = TextAnchor.MiddleLeft;
                style.padding = new RectOffset(10, 5, 5, 5);

                return style;
            }
        }
        public static GUIStyle NodeSelectedAura
        {
            get
            {
                GUIStyle style = new GUIStyle();

                //Background
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/RectangleSelected.png") as Texture2D;
                style.border = new RectOffset(15, 15, 15, 15);

                return style;
            }
        }
        public static GUIStyle NodeActiveAura
        {
            get
            {
                GUIStyle style = new GUIStyle();

                //Background
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/RectangleActive.png") as Texture2D;
                style.border = new RectOffset(15, 15, 15, 15);

                return style;
            }
        }
        public static GUIStyle NodeIOWhite
        {
            get
            {
                GUIStyle style = new GUIStyle();

                //Background
                style.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/NodeIO.png") as Texture2D;

                return style;
            }
        }
        public static GUIStyle NodeIOInputLabel
        {
            get
            {
                GUIStyle style = new GUIStyle();

                //Text
                style.fontSize = 12;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.white;
                style.alignment = TextAnchor.MiddleLeft;
                style.padding = new RectOffset(25, 10, 0, 0);

                return style;
            }
        }
        public static GUIStyle NodeIOOutputLabel
        {
            get
            {
                GUIStyle style = new GUIStyle();

                //Text
                style.fontSize = 12;
                style.fontStyle = FontStyle.Bold;
                style.normal.textColor = Color.white;
                style.alignment = TextAnchor.MiddleRight;
                style.padding = new RectOffset(10, 10, 0, 0);

                return style;
            }
        }


        //Asset Importer
        public static GUIStyle AssetOpenStyle { get
            {
                GUIStyle style = new GUIStyle();
                return style;
            } }
    }
}