using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour
{
    public class Node
    {
        //Node Data
        [Serializable]
        public class NodeData
        {
            public string NodeName = "NAME";

            //Serialize
            public void Serialize(Stream stream, IFormatter formatter)
            {
                formatter.Serialize(stream, NodeName);
            }

            //Deserialize
            public void Deserialize(Stream stream, IFormatter formatter)
            {
                NodeName = (string)formatter.Deserialize(stream);
            }
        }
        public NodeData data;

        //Graph Positioning
        [Serializable]
        public class NodeEditorData {

            //Position
            public Vector2 graphPosition = Vector2.zero;

            public NodeEditorData()
            {

            }
            public NodeEditorData(Vector2 graphPosition)
            {
                this.graphPosition = graphPosition;
            }

            //Serialize
            public void Serialize(Stream stream, IFormatter formatter)
            {
                formatter.Serialize(stream, graphPosition.x);
                formatter.Serialize(stream, graphPosition.y);
            }

            //Deserialize
            public void Deserialize(Stream stream, IFormatter formatter)
            {
                graphPosition = new Vector2((float)formatter.Deserialize(stream), (float)formatter.Deserialize(stream));
            }
        }
        public NodeEditorData editorData;

        //Style
        public class NodeEditorStyle
        {
            //Style
            public GUIStyle guiStyle;

            public float width = 250;
            public float height = 100;
        }
        public NodeEditorStyle NodeStyle { get; protected set; }

        //Result
        public enum Result { Running, Failure, Success };

        //Graph
        public BehaviourGraph Graph;

        //IO
        public List<NodeIO> inputPorts = new List<NodeIO>();
        public List<NodeIO> outputPorts = new List<NodeIO>();
        public void AddIO(NodeIO io)
        {
            if (io.mode == NodeIOMode.In)
                inputPorts.Add(io);
            if (io.mode == NodeIOMode.Out)
                outputPorts.Add(io);
        }

        //Active
        protected bool isActive = false;

        //Configuration
        protected bool canBeDeleted = true;
        
        //Properties
        public bool CanBeDeleted { get { return canBeDeleted; } }
        public bool IsActive {  get { return isActive; } }

        public Node()
        {
            data = new NodeData();
            editorData = new NodeEditorData(Vector2.zero);

            ConfigureNode();
        }
        public Node(BehaviourGraph graph, NodeEditorData editorData)
        {
            //Data
            data = new NodeData();

            //Editor
            this.editorData = editorData;

            //Graph
            Graph = graph;

            ConfigureNode();
        }

        //Configuration
        protected virtual void ConfigureNode()
        {
            //Style
            NodeStyle = new NodeEditorStyle();
            NodeStyle.guiStyle = new GUIStyle();
            NodeStyle.guiStyle.normal.background = EditorGUIUtility.Load("Assets/Editor/Ignite/Resources/Rectangle1_Gray.png") as Texture2D;
            NodeStyle.guiStyle.border = new RectOffset(12, 12, 12, 12);
        }

        //Execute
        public virtual Result Execute()
        {
            return Result.Failure;
        }

        //Serialize
        public void Serialize(Stream stream, IFormatter formatter)
        {
            //Data
            data.Serialize(stream, formatter);

            //Editor
            editorData.Serialize(stream, formatter);
        }

        //Deserialize
        public void Deserialize(Stream stream, IFormatter formatter)
        {
            //Data
            data = new NodeData();
            data.Deserialize(stream, formatter);

            //Editor
            editorData = new NodeEditorData();
            editorData.Deserialize(stream, formatter);
        }
    }
}