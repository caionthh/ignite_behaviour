using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Ignite.Behaviour.Editor
{

    public class NodeIOEditor
    {
        //Rect
        public Rect rect;
        public NodeIO nodeIO;
        public NodeIOMode type;
        public NodeEditor node;

        //Action
        public Action<NodeIOEditor> OnClickConnectionPoint;

        //Point
        public NodeIOEditor(NodeIO nodeIO, NodeEditor node, NodeIOMode type, Action<NodeIOEditor> OnClickConnectionPoint)
        {
            this.nodeIO = nodeIO;
            this.node = node;
            this.type = type;
            this.OnClickConnectionPoint = OnClickConnectionPoint;
            rect = new Rect(0, 0, 20f, 20f);
        }


        //Draw
        public void Draw(int index)
        {
            //Y
            rect.y = node.contentRect.y + 15f + (30f  * index);

            Rect labelRect = new Rect(0,0, 50f, 20f);
            labelRect.y = rect.y;

            switch (type)
            {
                case NodeIOMode.In:

                    //Position on border
                    rect.x = node.contentRect.x - rect.width * .5f;

                    //Label
                    labelRect.x = rect.x;
                    GUI.Label(labelRect, nodeIO.label, BehaviourGraphStyles.NodeIOInputLabel);

                    break;

                case NodeIOMode.Out:

                    //Position on border
                    rect.x = node.contentRect.x + node.contentRect.width - rect.width * .5f;

                    //Label
                    labelRect.x = rect.x - labelRect.width;
                    GUI.Label(labelRect, nodeIO.label, BehaviourGraphStyles.NodeIOOutputLabel);

                    break;
            }

            if (GUI.Button(rect, "", BehaviourGraphStyles.NodeIOWhite))
            {
                if (OnClickConnectionPoint != null)
                    OnClickConnectionPoint(this);
            }
        }
    }
}