using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    public class NodeEditor
    {
        //Editor
        public BehaviourGraphEditor editor;

        //Reference
        public Node node;
        public int graphIndex = -1;

        //Rect
        public Rect nodeRect, contentRect, titleRect, selectedAura, activeAura;

        //Points
        protected List<NodeIOEditor> nodeInputPorts = new List<NodeIOEditor>();
        protected List<NodeIOEditor> nodeOutputPorts = new List<NodeIOEditor>();

        //Style
        public string title;
        public GUIStyle style;
        public bool isDragged;

        //Control
        public bool isMinimized = false;
        public bool isSelected = false;

        //Node
        public NodeEditor(Node node, Action<NodeIOEditor> OnClickInPoint, Action<NodeIOEditor> OnClickOutPoint)
        {
            //Reference
            //this.editor = editor;
            //this.graphIndex = graphIndex;
            this.node = node;

            PopulateIO(OnClickInPoint, OnClickOutPoint);

            //Style
            style = node.NodeStyle.guiStyle;
        }

        //Populate IO
        protected void PopulateIO(Action<NodeIOEditor> OnClickInPoint, Action<NodeIOEditor> OnClickOutPoint)
        {
            foreach(NodeIO port in node.inputPorts)
            {
                NodeIOEditor point;
                point = new NodeIOEditor(port, this, NodeIOMode.In, OnClickInPoint);

                nodeInputPorts.Add(point);
            }
            foreach (NodeIO port in node.outputPorts)
            {
                NodeIOEditor point;
                point = new NodeIOEditor(port, this, NodeIOMode.Out, OnClickOutPoint);

                nodeOutputPorts.Add(point);
            }                
        }

        //Drag
        public void Drag(Vector2 delta)
        {
            node.editorData.graphPosition += delta;
        }

        //Update
        public void Update(Node baseNode)
        {
            this.node = baseNode;
        }

        //Draw
        public void Draw(Vector2 gridOffset)
        {
            //GraphPosition
            Vector2 position = node.editorData.graphPosition + gridOffset;

            //Node rect
            nodeRect = new Rect(position.x, position.y, node.NodeStyle.width, node.NodeStyle.height + 45);

            //Aura
            selectedAura = new Rect(nodeRect.position.x - 5, nodeRect.position.y - 5, nodeRect.width + 10, nodeRect.height + 10);
            activeAura = new Rect(nodeRect.position.x - 8, nodeRect.position.y - 8, nodeRect.width + 16, nodeRect.height + 16);
            if (node.IsActive)
                GUI.Box(activeAura, "", BehaviourGraphStyles.NodeActiveAura);
            if (isSelected)
                GUI.Box(selectedAura, "", BehaviourGraphStyles.NodeSelectedAura);

            //Rect
            contentRect = new Rect(position.x, position.y + 45, node.NodeStyle.width, node.NodeStyle.height);
            GUI.Box(contentRect, "", style);

            //Title Rect
            titleRect = new Rect(position.x, position.y, node.NodeStyle.width, 50);
            GUI.Box(titleRect, node.data.NodeName, BehaviourGraphStyles.NodeTitleStyle);

            //GUI.Box(nodeRect, "", BehaviourGraphStyles.NodeTitleStyle);

            //Points
            for (int i = 0; i < nodeInputPorts.Count; i++)
                nodeInputPorts[i].Draw(i);
            for (int i = 0; i < nodeOutputPorts.Count; i++)
                nodeOutputPorts[i].Draw(i);


            //Title
            //GUI.Label(new Rect(position.x, position.y, node.NodeStyle.width, 50), node.data.NodeName, BehaviourGraphStyles.NodeTitleStyle);
        }

        //Events
        public bool ProcessEvents(Event e)
        {
            switch (e.type) {

                //Down
                case EventType.MouseDown:
                    //Left Click
                    if (e.button == 0)
                    {
                        if (nodeRect.Contains(e.mousePosition))
                        {
                            isDragged = true;
                            GUI.changed = true;
                        }
                        else
                        {
                            GUI.changed = true;
                        }
                    }
                    //Right Click
                    if (e.button == 1)
                    {
                        if (nodeRect.Contains(e.mousePosition))
                        {
                            ProcessContextMenu();
                        }
                    }
                    break;

                    //Mouse Up
                case EventType.MouseUp:
                    isDragged = false;
                    break;

                    //Drag
                case EventType.MouseDrag:
                    if (e.button == 0 && isDragged)
                    {
                        Drag(e.delta);
                        e.Use();
                        return true;
                    }
                    break;
            
            }


            return false;
        }

        //Context
        public void ProcessContextMenu()
        {
            GenericMenu menu = new GenericMenu();
            if (node.CanBeDeleted)
                menu.AddItem(new GUIContent("Delete"), false, () => RemoveFromGraph());
            menu.ShowAsContext();
        }

        //Remove From Graph
        public void RemoveFromGraph()
        {
            node.Graph.RemoveNode(node);
        }
    }
}