using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{
    Coroutine coroutine;

    public int coroutineInt = 0;
    public bool startedCoroutine = false;

    // Start is called before the first frame update
    void Start()
    {
        startedCoroutine = true;
        Debug.Log("Start: " + startedCoroutine);
        coroutine = StartCoroutine(Count());
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update: " + startedCoroutine);
    }

    //Debug
    public IEnumerator Count()
    {
        Debug.Log("Inside: " + startedCoroutine);
        startedCoroutine = false;
        yield return null;
    }
}
