using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor;
using UnityEngine;


namespace Ignite.Behaviour
{
    public class BehaviourGraph
    {
        //File Format
        public const string FILE_FORMAT = "ibgraph";

        //Graph Data
        [Serializable]
        public class BehaviourGraphData {
            
            //Name
            public string graphName = "NAME";

            //Nodes
            public List<Node> graphNodes = new List<Node>();
            public Node RootNode { get { return graphNodes[0]; } }

            //Blackboard
            public Dictionary<string, object> Blackboard { get; set; }

            //Serialize
            public void Serialize(Stream stream, IFormatter formatter)
            {
                //Name
                formatter.Serialize(stream, graphName);

                //Nodes
                formatter.Serialize(stream, graphNodes.Count);
                foreach (Node node in graphNodes)
                {
                    formatter.Serialize(stream, node.GetType());
                    node.Serialize(stream, formatter);
                }

                //BlackBoard
                //formatter.Serialize(stream, Blackboard.Count);
                //formatter.Serialize(stream, Blackboard);
            }

            //Deserialize
            public void Deserialize(Stream stream, IFormatter formatter, BehaviourGraph graph)
            {
                //Name
                graphName = (string)formatter.Deserialize(stream);

                //Nodes
                int nodeCount = (int)formatter.Deserialize(stream);
                graphNodes.Clear();
                for(int i = 0; i < nodeCount; i++)
                {
                    //Type
                    Type nodeType = (Type)formatter.Deserialize(stream);
                    //Node node = new Node(graph, new Node.NodeEditorData(Vector2.zero));
                    object node = Activator.CreateInstance(nodeType);
                    ((Node)node).Deserialize(stream, formatter);
                    ((Node)node).Graph = graph;
                    graphNodes.Add((Node)node);
                }

                //Blackboard
                //Blackboard = (Dictionary<string, object>)formatter.Deserialize(stream);
            }
        }
        public BehaviourGraphData data { get; protected set; }

        //Name
        public string GraphName { get { return data.graphName; } set { data.graphName = value; } }

        //File
        private string path = "";
        public bool HasPath { get { return path.Length > 0; } }
        public string Path { get { return path; } }

        //Nodes
        public List<Node> GraphNodes
        {
            get { return data.graphNodes; }
            set { 
                data.graphNodes = value;
                if (OnGraphNodesChanged != null) OnGraphNodesChanged(GraphNodes);
            }
        }
        public void AddNode(Node node)
        {
            data.graphNodes.Add(node);
            if (OnGraphNodeAdd != null)
                OnGraphNodeAdd(node);
        }
        public void RemoveNode(Node node)
        {
            if (!node.CanBeDeleted)
                return;

            data.graphNodes.Remove(node);
            if (OnGraphNodeRemove != null)
                OnGraphNodeRemove(node);
        }
        public Action<List<Node>> OnGraphNodesChanged;
        public Action<Node> OnGraphNodeAdd;
        public Action<Node> OnGraphNodeRemove;

        //Coroutine
        protected Coroutine GraphExecution;
        public bool startedBehaviour = false;

        //Root
        private NodeRoot root;
        public NodeRoot Root {  get { return root; } }

        //GRaph
        public BehaviourGraph()
        {
            //Data
            data = new BehaviourGraphData();

            //Root
            root = new NodeRoot(this, new Node.NodeEditorData(Vector2.zero));
            AddNode(root);

            //Coroutine
            startedBehaviour = false;
        }

        public void RunBehaviour(MonoBehaviour owner)
        {
            if (!startedBehaviour)
            {
                GraphExecution = owner.StartCoroutine(ExecuteBehaviour());
            }
        }

        //Execute Behaviour
        private IEnumerator ExecuteBehaviour()
        {
            //Started
            startedBehaviour = true;

            //Execute Root
            Node.Result result = data.RootNode.Execute();

            //Running
            while (result == Node.Result.Running){
                Debug.Log("Root Result: " + result.ToString());
                yield return new WaitForSeconds (.1f);
                result = root.Execute();
            }

            //Finished
            Debug.Log("Behaviour has finished with: " + result.ToString());
            startedBehaviour = false;

            yield return null;
        }

        //Save Graph
        public void SaveGraph()
        {
            Stream stream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);

            try
            {
                //string json = JsonUtility.ToJson(data);
                //File.WriteAllText(path, json);
                IFormatter formatter = new BinaryFormatter();

                //Path
                formatter.Serialize(stream, path);

                //Serialize Data
                data.Serialize(stream, formatter);

                stream.Close();
            }
            catch(Exception e)
            {
                Debug.LogError(e.Message);
                stream.Close();
            }

        }
        public void SaveGraph(string path)
        {
            //Path
            this.path = path;
            SaveGraph();
        }

        //Create from File
        public static BehaviourGraph FromFile(string path)
        {
            try
            {
                //string json = File.ReadAllText(path);
                //graph.data = JsonUtility.FromJson<BehaviourGraphData>(json);
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

                //Graph
                BehaviourGraph graph = new BehaviourGraph();

                //Path
                graph.path = (string)formatter.Deserialize(stream);

                //Data
                graph.data.Deserialize(stream, formatter, graph);

                stream.Close();

                return graph;
            }
            catch(Exception e)
            {
                Debug.LogError(e.Message);
            }

            return null;
        }
        
        //Copy
        //public static BehaviourGraph Copy(BehaviourGraph from)
        //{
        //    BehaviourGraph graph = new BehaviourGraph();
        //    graph.data = from.data;
        //    from.GraphNodes.CopyTo(graph.GraphNodes);
        //}

    }
}