using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    [CustomEditor(typeof(BehaviourGraphAsset))]
    public class BehaviourGraphAssetEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            //Open in Editor
            GUILayoutUtility.GetRect(Screen.width, 70);
            if (GUI.Button(new Rect(5, 5, Screen.width - 10, 50), "Open in Editor"))
            {
                BehaviourGraphEditor.OpenWindow(AssetDatabase.GetAssetPath(Selection.activeObject));
            }
        }
    }
}