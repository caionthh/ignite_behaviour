using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    [CustomEditor(typeof(BehaviourGraphComponent))]
    public class BehaviourGraphComponentEditor : UnityEditor.Editor
    {
        //Component
        private BehaviourGraphComponent component;

        //Data
        private SerializedProperty path;
        private SerializedProperty enableExecution;

        //Enable
        private void OnEnable()
        {
            
        }

        //Inspector
        public override void OnInspectorGUI()
        {
            //Data
            component = (BehaviourGraphComponent)target;
            path = serializedObject.FindProperty("graphPath");
            enableExecution = serializedObject.FindProperty("enableExecution");

            //Debug
            GUILayout.Label(serializedObject.FindProperty("graphPath").stringValue);

            //Import Graph
            if (string.IsNullOrEmpty(path.stringValue))
            {
                GUILayout.Label("No Behaviour Graph found.");
                GUILayout.Label("Open or Drag and Drop below.");
                ImportGraphWindow();
            }
            //Behaviour Component Inspector
            else
            {
                //Change Graph
                GUILayout.Label("Change Graph");
                ImportGraphWindow();

                //Build Graph
                if (component.graph == null)
                    component.LoadGraph();

                Rect openEditor = GUILayoutUtility.GetRect(0, 40, GUILayout.ExpandWidth(true));
                if (GUI.Button(openEditor, "Open in Editor"))
                {
                    BehaviourGraphEditor.OpenWindow(component.graphPath);
                }

                //Executing
                if (component.graph.startedBehaviour)
                    GUILayout.Label("Graph is running.");
                else
                    GUILayout.Label("Graph is stopped.");

                Rect enableRect = GUILayoutUtility.GetRect(0, 40, GUILayout.ExpandWidth(true));
                if (!enableExecution.boolValue)
                {
                    if (GUI.Button(enableRect, "Enable Execution"))
                        enableExecution.boolValue = true;
                }
                else
                {
                    if (GUI.Button(enableRect, "Disable Execution"))
                        enableExecution.boolValue = false;
                }
                Rect executeRect = GUILayoutUtility.GetRect(0, 40, GUILayout.ExpandWidth(true));
                if (GUI.Button(executeRect, "Execute Once"))
                {
                    component.ExecuteGraph();
                }
            }

            //Changed
            if (GUI.changed)
            {
                serializedObject.ApplyModifiedProperties();
                EditorUtility.SetDirty(target);
                Repaint();
            }
        }

        //Import Graph
        private void ImportGraphWindow()
        {
            //Drag and Drop Area
            Rect dropRect = GUILayoutUtility.GetRect(0, 80, GUILayout.ExpandWidth(true));
            GUI.Box(dropRect, "Drag and Drop");
            if (dropRect.Contains(Event.current.mousePosition))
            {
                if (Event.current.type == EventType.DragUpdated)
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    Event.current.Use();
                }
                else if (Event.current.type == EventType.DragPerform)
                {
                    //DragAndDrop.objectReferences[0]
                    string filePath = DragAndDrop.paths[0];
                    string format = Path.GetExtension(filePath).Trim('.');
                    if (format.Equals(BehaviourGraph.FILE_FORMAT))
                    {
                        path.stringValue = filePath;
                        serializedObject.ApplyModifiedProperties();
                        component.LoadGraph();
                    }
                }
            }
        }

    }
}