using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    public class ConnectionEditor
    {
        //Connection
        public NodeIOEditor InPoint;
        public NodeIOEditor OutPoint;
        public Action<ConnectionEditor> OnClickRemoveConnection;

        public ConnectionEditor(NodeIOEditor inPoint, NodeIOEditor outPoint, Action<ConnectionEditor> OnClickRemoveConnection)
        {
            InPoint = inPoint;
            OutPoint = outPoint;
            this.OnClickRemoveConnection = OnClickRemoveConnection;
        }

        //Draw
        public void Draw()
        {
            Handles.DrawBezier(
                InPoint.rect.center,
                OutPoint.rect.center,
                InPoint.rect.center + Vector2.left * 50f,
                OutPoint.rect.center - Vector2.left * 50f,
                Color.white,
                null,
                2f
                );

            if (Handles.Button((InPoint.rect.center + OutPoint.rect.center) * 0.5f, Quaternion.identity, 4, 8, Handles.RectangleHandleCap))
            {
                if (OnClickRemoveConnection != null)
                {
                    OnClickRemoveConnection(this);
                }
            }
        }
    }
}