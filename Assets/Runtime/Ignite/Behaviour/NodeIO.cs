using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ignite.Behaviour
{
    public enum NodeIOMode { In, Out }

    public class NodeIO
    {
        //Type
        public NodeIOMode mode;

        //Label
        public string label;

        //Parent
        public List<Node> ConnectedTo = new List<Node>();

        //Node
        public NodeIO(string label, NodeIOMode mode)
        {
            this.label = label;
            this.mode = mode;
        }
    }
}