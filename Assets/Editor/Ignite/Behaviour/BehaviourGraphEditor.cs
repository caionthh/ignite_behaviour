using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Ignite.Behaviour.Editor
{
    public class BehaviourGraphEditor : EditorWindow
    {
        [MenuItem("Window/Ignite/AI/Behaviour Tree")]
        private static BehaviourGraphEditor OpenWindow()
        {
            BehaviourGraphEditor window = GetWindow<BehaviourGraphEditor>();
            window.titleContent = new GUIContent("Behaviour Graph Editor");
            return window;
        }
        public static void OpenWindow(string filePath)
        {
            OpenWindow().LoadFromFile(filePath);
        }

        //Source
        private BehaviourGraph activeGraph;
        private string inactiveGraphPath;
        private BehaviourGraphComponent graphExecutingComponent;

        //Component
        public BehaviourGraphComponent GraphExecutingComponent { get { return graphExecutingComponent; } }

        //Mode
        private bool isOnPlayMode = false;

        //Nodes
        private List<NodeEditor> nodes;
        private List<ConnectionEditor> connections;
        public Node GetNodeFromGraph(int index)
        {
            return activeGraph.GraphNodes[index];
        }

        //Grid
        private Vector2 graphOffset;
        private Vector2 gridDrag;

        //Toolbar
        private int toolbar;

        //Functions
        private bool isDragged;

        //Connection
        private NodeIOEditor selectedInPoint, selectedOutPoint;

        //Selection
        private List<object> selection = new List<object>();
        private bool multiSelection = false;
        public void SetSelected(object obj)
        {
            ClearSelection();

            AddToSelection(obj);
        }
        public void AddToSelection(object obj)
        {
            selection.Add(obj);
        }
        public void ClearSelection()
        {
            foreach(object obj in selection)
            {
                if (obj is NodeEditor)
                    ((NodeEditor)obj).isSelected = false;
            }

            selection.Clear();
        }
        public void RemoveFromSelection(object obj)
        {
            selection.Remove(obj);
        }

        //Enable
        private void OnEnable()
        {
            //Reset Offset
            ResetGridOffset();

            //Create Default Project
            CreateNewGraph("BehaviourGraph");

            //Play mode
            if (EditorApplication.isPlaying)
            {
                isOnPlayMode = true;
            }
            EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
        }

        //Playmode Changed
        private void EditorApplication_playModeStateChanged(PlayModeStateChange stateChanged)
        {

            if (stateChanged == PlayModeStateChange.EnteredPlayMode)
            {
                //Save
                SaveAlert();

                //Graph
                if (activeGraph != null)
                    inactiveGraphPath = activeGraph.Path;
                activeGraph = null;

                isOnPlayMode = true;
            }
            
            if (stateChanged == PlayModeStateChange.EnteredEditMode)
            {
                if (!string.IsNullOrEmpty(inactiveGraphPath))
                {
                    activeGraph = BehaviourGraph.FromFile(inactiveGraphPath);
                    inactiveGraphPath = "";
                    ReloadGraphNodes();
                }

                isOnPlayMode = false;
            }
        }

        //On GUI
        private void OnGUI()
        {
            //Grid
            DrawGrid(20, .2f, Color.gray);
            DrawGrid(100, .4f, Color.gray);
            DrawGrid(1000, .6f, Color.gray);

            //Execution Graph
            if (isOnPlayMode)
            {
                if (Selection.activeGameObject != null)
                {
                    //Have Selected
                    graphExecutingComponent = ((GameObject)Selection.activeGameObject).GetComponent<BehaviourGraphComponent>();
                    if (graphExecutingComponent && (activeGraph == null || activeGraph != graphExecutingComponent.graph))
                    {
                        Debug.Log("Changed Graph");
                        activeGraph = graphExecutingComponent.graph;
                        ReloadGraphNodes();
                    }
                    else if (graphExecutingComponent == null)
                    {
                        activeGraph = null;
                        ReloadGraphNodes();
                    }

                }
                else
                {
                    activeGraph = null;
                    ReloadGraphNodes();
                }

            }

            //Nodes
            DrawNodes();
            DrawConnections();

            //Top Bar
            toolbar = GUI.Toolbar(new Rect(0, 0, position.width, 30), -1, new string[] { "New Graph", "Load", "Save", "Save as" });

            //Title
            GUI.Label(new Rect(10, 35, position.width, 50),
                "Behaviour Graph"
                + (activeGraph != null ? " >> " + activeGraph.GraphName : "")
                + (isOnPlayMode ? "(Playing)" : ""), BehaviourGraphStyles.GraphTitleStyle);
            GUI.Label(new Rect(15, 65, position.width, 50), "Nodes >> " + (nodes != null ? nodes.Count.ToString() : "0") + " (" + (activeGraph != null ? activeGraph.GraphNodes.Count.ToString() : "0") + ")" , BehaviourGraphStyles.GraphSubtitleStyle);

            //Process
            ProcessNodeEvents(Event.current);
            ProcessEvents(Event.current);

            //Update Graph

            //Toolbar
            ProcessToolbar();

            //Changed
            if (GUI.changed)
                Repaint();
        }

        //Toolbar
        protected void ProcessToolbar()
        {
            switch (toolbar) {

                case 0:
                    OpenNewGraphWindow();
                    break;
                case 1:
                    OpenGraph();
                    break;
                case 2:
                    SaveGraph();
                    break;
                case 3:
                    SaveAsGraph();
                    break;

                default:
                    break;
            
            }
        }

        //Create Graph
        private void OpenNewGraphWindow()
        {
            //Alert if need to save
            SaveAlert();

            //graph = new BehaviourGraph();
            //New Graph Popup
            BehaviourNewGraphEditor.ShowPopup(this);
        }
        public void CreateNewGraph(string name)
        {
            activeGraph = new BehaviourGraph();
            activeGraph.GraphName = name;
            BindGraphEvents();

            //Reload
            ReloadGraphNodes();
        }

        //Open Graph
        private void OpenGraph()
        {
            //Alert if save is needed
            SaveAlert();

            //Load
            var path = EditorUtility.OpenFilePanel("Open Graph", "Assets", BehaviourGraph.FILE_FORMAT);
            if (!String.IsNullOrEmpty(path))
            {
                activeGraph = BehaviourGraph.FromFile(path);
                BindGraphEvents();
                ReloadGraphNodes();
            }
        }

        //Save Alert
        private void SaveAlert()
        {
            if (activeGraph != null)
            {
                if (EditorUtility.DisplayDialog("Save", "Save current open graph?", "Yes", "No"))
                {
                    SaveGraph();
                }
            }
        }

        //Save Graph
        private void SaveGraph()
        {
            try
            {
                if (activeGraph != null)
                {
                    if (activeGraph.HasPath)
                        activeGraph.SaveGraph();
                    else
                    {
                        var path = EditorUtility.SaveFilePanelInProject("Save", "behaviourgraph", BehaviourGraph.FILE_FORMAT, "Behaviour Graph");
                        if (!String.IsNullOrEmpty(path))
                            activeGraph.SaveGraph(path);
                        AssetDatabase.Refresh();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        private void SaveAsGraph()
        {
            try
            {
                if (activeGraph != null)
                {
                    var path = EditorUtility.SaveFilePanelInProject("Save as", "behaviourgraph", BehaviourGraph.FILE_FORMAT, "Behaviour Graph");
                    if (!String.IsNullOrEmpty(path))
                        activeGraph.SaveGraph(path);
                    AssetDatabase.Refresh();
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }

        //Load From File
        public void LoadFromFile(string path)
        {
            activeGraph = BehaviourGraph.FromFile(path);
            BindGraphEvents();

            //Reload
            ReloadGraphNodes();
        }
        
        //Load Nodes
        public void ReloadGraphNodes()
        {
            //Nodes
            if (nodes != null)
                nodes.Clear();
            else
                nodes = new List<NodeEditor>();

            //Connections
            if (connections != null)
                connections.Clear();
            else
                connections = new List<ConnectionEditor>();

            if (activeGraph == null)
                return;

            //Add Nodes
            foreach (Node node in activeGraph.GraphNodes)
                AddGraphNode(node);
        }


        //Bind Events
        protected void BindGraphEvents()
        {
            //Changed
            activeGraph.OnGraphNodesChanged = OnGraphNodesChanged;
            activeGraph.OnGraphNodeAdd = OnGraphNodeAdd;
            activeGraph.OnGraphNodeRemove = OnGraphNodeRemove;
        }
        private void OnGraphNodesChanged(List<Node> nodes)
        {
            Debug.Log("Nodes Changed");
        }
        private void OnGraphNodeAdd(Node node)
        {
            //AddGraphNode(node);
        }
        private void OnGraphNodeRemove(Node node)
        {
            ReloadGraphNodes();
        }
        //Nodes
        private void AddGraphNode(Node node)
        {
            try
            {
                nodes.Add(new NodeEditor(
                    node,
                    OnClickInPoint,
                    OnClickOutPoint));
            }
            catch (Exception e)
            {
                Debug.LogError(e.Message);
            }
        }
        private void NodeUpdatePlaymode()
        {
            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    if (activeGraph != null)
                        nodes[i].Update(graphExecutingComponent.graph.GraphNodes[i]);
                }
            }
        }
        private void DrawNodes()
        {
            if (nodes != null)
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    nodes[i].Draw(graphOffset);
                }
            }
        }
        private void DrawConnections()
        {
            if (connections != null)
            {
                for (int i = 0; i < connections.Count; i++)
                {
                    connections[i].Draw();
                }
            }
        }
        

        //Events
        private void ProcessNodeEvents(Event e)
        {
            if (nodes != null)
            {
                for (int i = nodes.Count-1; i>=0; i--)
                {
                    bool guiChanged = nodes[i].ProcessEvents(e);

                    if (guiChanged)
                        GUI.changed = true;
                }
            }
        }
        private void ProcessEvents(Event e)
        {
            switch (e.type)
            {
                //Multi
                case EventType.KeyDown:
                    //Multi Selection
                    if (e.keyCode == KeyCode.LeftShift)
                        multiSelection = true;
                    break;
                case EventType.KeyUp:
                    //Multi Selection
                    if (e.keyCode == KeyCode.LeftShift)
                        multiSelection = false;
                    break;

                case EventType.MouseDown:
                    //Left Button
                    if (e.button == 0)
                    {
                        //Check if clicked any node
                        bool clickedNode = false;
                        foreach (NodeEditor node in nodes)
                            if (node.nodeRect.Contains(e.mousePosition))
                            {
                                clickedNode = true;

                                if (multiSelection)
                                    AddToSelection(node);
                                else
                                    SetSelected(node);

                                node.isSelected = true;
                                break;
                            }

                        if (!clickedNode)
                        {
                            //Remove Selection
                            ClearSelection();

                            //Clear Nodes
                            foreach (NodeEditor node in nodes)
                                node.isSelected = false;
                        }
                    }

                    //Right Button
                    if (e.button == 1)
                    {
                        //Check if clicked any node
                        bool clickedNode = false;
                        foreach (NodeEditor node in nodes)
                            if (node.nodeRect.Contains(e.mousePosition))
                            {
                                node.ProcessContextMenu();
                                clickedNode = true;
                                break;
                            }

                        //Check
                        if (!clickedNode)
                            ProcessContextMenu(e.mousePosition);
                    }
                    if (e.button == 2)
                    {
                        isDragged = true;
                    }
                    break;

                case EventType.MouseUp:
                    if (e.button == 2)
                        isDragged = false;
                    break;

                case EventType.MouseDrag:
                    if (isDragged && e.button == 2)
                    {
                        DragGrid(e.delta);
                        GUI.changed = true;
                    }
                    break;

            }
        }

        //Context Menu
        private void ProcessContextMenu(Vector2 mousePosition)
        {
            GenericMenu genericMenu = new GenericMenu();
            if (selection.Count > 0)
                genericMenu.AddItem(new GUIContent("Delete selection"), false, () => DeleteSelection());
            genericMenu.AddItem(new GUIContent("Add node"), false, () => OnClickAddNode(mousePosition));
            genericMenu.AddItem(new GUIContent("Go to origin"), false, () => ResetGridOffset());
            genericMenu.ShowAsContext();
        }

        //Delete Selection
        private void DeleteSelection()
        {
            if (selection.Count > 0)
            {
                foreach (object obj in selection)
                {
                    //Nodes
                    if (obj is NodeEditor)
                    {
                        NodeEditor node = (NodeEditor)obj;
                        node.RemoveFromGraph();
                    }
                }
            }
        }

        //Add Node
        private void OnClickAddNode(Vector2 mousePosition)
        {
            if (activeGraph != null)
            {
                activeGraph.AddNode(new Node(activeGraph, new Node.NodeEditorData(mousePosition - graphOffset)));
            }
        }

        //Connection Point
        private void OnClickInPoint(NodeIOEditor inPoint)
        {
            selectedInPoint = inPoint;

            if (selectedOutPoint != null)
            {
                if (selectedOutPoint.node != selectedInPoint.node)
                {
                    CreateConnection();
                    ClearConnectionSelection();
                }
                else
                {
                    ClearConnectionSelection();
                }
            }
        }

        private void OnClickOutPoint(NodeIOEditor outPoint)
        {
            selectedOutPoint = outPoint;

            if (selectedInPoint != null)
            {
                if (selectedOutPoint.node != selectedInPoint.node)
                {
                    CreateConnection();
                    ClearConnectionSelection();
                }
                else
                {
                    ClearConnectionSelection();
                }
            }
        }

        private void OnClickRemoveConnection(ConnectionEditor connection)
        {
            connections.Remove(connection);
        }

        private void CreateConnection()
        {
            if (connections == null)
                connections = new List<ConnectionEditor>();

            connections.Add(new ConnectionEditor(selectedInPoint, selectedOutPoint, OnClickRemoveConnection));
        }

        private void ClearConnectionSelection()
        {
            selectedInPoint = null;
            selectedOutPoint = null;
        }

        //Grid
        private void DrawGrid(float spacing, float opacity, Color gridColor)
        {
            int wDiv = Mathf.CeilToInt(position.width / spacing);
            int hDiv = Mathf.CeilToInt(position.height / spacing);

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, opacity);

            graphOffset += gridDrag * .5f;
            Vector3 newOffset = new Vector3(graphOffset.x % spacing, graphOffset.y % spacing, 0);

            //Width
            for (int i = 0; i < wDiv; i++)
            {
                Handles.DrawLine(
                    new Vector3(spacing * i, -spacing, 0) + newOffset,
                    new Vector3(spacing * i, position.height, 0) + newOffset);
            }
            //Height
            for (int j = 0; j < hDiv; j++)
            {
                Handles.DrawLine(
                    new Vector3(-spacing, spacing * j, 0) + newOffset,
                    new Vector3(position.width, spacing * j, 0) + newOffset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }
           
        //Drag Grid
        private void DragGrid(Vector2 delta)
        {
            graphOffset += delta;
        }
        private void ResetGridOffset()
        {
            graphOffset = new Vector2(position.width / 2.0f, position.height / 2.0f);
        }
    
    }
}