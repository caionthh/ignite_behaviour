using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ignite.Behaviour
{
    public class NodeRoot : Node
    {
        //Root Node
        public NodeRoot() : base ()
        {

        }
        public NodeRoot(BehaviourGraph graph, NodeEditorData editorData) : base(graph, editorData)
        {
            
        }

        //Configure
        protected override void ConfigureNode()
        {
            base.ConfigureNode();

            //Name
            data.NodeName = "Root";

            //Style
            NodeStyle.width = 100;
            NodeStyle.height = 50;

            //Deleted
            canBeDeleted = false;

            //Add Output
            AddIO(new NodeIO("Execute", NodeIOMode.Out));
        }

        //Execute
        public override Result Execute()
        {
            isActive = true;
            Debug.Log(IsActive);
            return Result.Failure;
        }
    }
}