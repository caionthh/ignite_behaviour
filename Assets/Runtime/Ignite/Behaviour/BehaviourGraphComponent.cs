using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ignite.Behaviour
{
    public class BehaviourGraphComponent : MonoBehaviour
    {
        //Graph
        [SerializeField]
        public string graphPath;
        public BehaviourGraph graph;

        //Execute
        public bool enableExecution = false;

        //Path
        public void SetPath(string path)
        {
            graphPath = path;
        }

        //Awake
        private void Awake()
        {

        }

        //Enable
        private void OnEnable()
        {
            //Load Graph
            if (!string.IsNullOrEmpty(graphPath))
                LoadGraph();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //Execute
            if (enableExecution)
                ExecuteGraph();
        }

        //Load Graph
        public void LoadGraph()
        {
            graph = BehaviourGraph.FromFile(graphPath);
        }

        //Execute
        public void ExecuteGraph()
        {
            if (enableExecution && graph != null)
            {
                graph.RunBehaviour(this);
            }
        }
    }
}