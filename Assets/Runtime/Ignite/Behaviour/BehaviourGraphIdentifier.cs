using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ignite.Behaviour
{
    public class BehaviourGraphIdentifier : ScriptableObject
    {
        public string GraphPath;
    }
}